<?php require_once 'header_idx.php'; ?>

<link rel="stylesheet" href="assets/css1/index1.css">

<div class="link-box-container">
    <div class="link-box">
        <a href="productos/index.php">Productos</a>
    </div>

    <div class="link-box">
        <a href="compras/index.php">Compras</a>
    </div>

    <div class="link-box">
        <a href="ventas/index.php">Ventas</a>
    </div>

    <div class="link-box">
        <a href="stock">Stock</a>
    </div>
</div>

<?php require_once 'footer.php'; ?>